storm (1.0-1) unstable; urgency=medium

  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Wed, 17 Jul 2024 10:27:03 +0100

storm (0.26-3) unstable; urgency=medium

  * Build with all hardening options.
  * Use pybuild-plugin-pyproject.
  * Add Testsuite: autopkgtest-pkg-pybuild.
  * Standards-Version: 4.7.0 (no changes required).

 -- Colin Watson <cjwatson@debian.org>  Fri, 12 Jul 2024 10:53:17 +0100

storm (0.26-2) unstable; urgency=medium

  * Build-depend on python3-six (closes: #1064699).

 -- Colin Watson <cjwatson@debian.org>  Mon, 26 Feb 2024 10:56:13 +0000

storm (0.26-1) unstable; urgency=medium

  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Wed, 06 Dec 2023 23:17:26 +0000

storm (0.25-2) unstable; urgency=medium

  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.2, no changes needed.
  * Clean .db.lock and db/* (closes: #1046671).

 -- Colin Watson <cjwatson@debian.org>  Mon, 14 Aug 2023 12:58:17 +0100

storm (0.25-1) unstable; urgency=medium

  * New upstream release, restoring MySQL support.
  * Remove python3-storm-dbg in favour of automatically-generated -dbgsym
    packages (closes: #994382).

 -- Colin Watson <cjwatson@debian.org>  Thu, 23 Sep 2021 11:59:56 +0100

storm (0.24-2) unstable; urgency=low

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Colin Watson <cjwatson@debian.org>  Tue, 10 Nov 2020 00:11:39 +0000

storm (0.24-1) unstable; urgency=medium

  * debian/watch: Generalise slightly using @ARCHIVE_EXT@.
  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Fri, 12 Jun 2020 12:08:02 +0100

storm (0.23-1) unstable; urgency=medium

  * New upstream release.
  * Remove alternate Suggests on python3-mysqldb | python3-pysqlite2 from
    python3-storm-dbg, to match python3-storm.  MySQL support was removed in
    0.21, and SQLite support is provided by Python 3's standard library.
  * Remove mention of MySQL from package descriptions.

 -- Colin Watson <cjwatson@debian.org>  Wed, 18 Mar 2020 17:52:21 +0000

storm (0.22-3) unstable; urgency=medium

  * debian/copyright: Update copyright dates to 2019.
  * Remove redundant versioning on python3-all-* build-dependencies.

 -- Colin Watson <cjwatson@debian.org>  Thu, 26 Dec 2019 01:36:24 +0000

storm (0.22-2) unstable; urgency=medium

  * No-change reupload due to dgit signing confusion.

 -- Colin Watson <cjwatson@debian.org>  Thu, 21 Nov 2019 16:05:09 +0000

storm (0.22-1) unstable; urgency=medium

  * Convert git repository from git-dpm to gbp layout.
  * Fix typos in package descriptions.
  * Update watch file to download GPG signatures.
  * Restore DPMT as Maintainer and set myself as Uploader (closes: #940876).
  * New upstream release (closes: #725462).
  * Adjust packaging for port to Python 3.
  * Switch to pybuild.
  * Set Rules-Requires-Root: no.
  * Policy version 4.4.1.
  * Restore DPMT Vcs-* fields.
  * Update Homepage to use HTTPS.

 -- Colin Watson <cjwatson@debian.org>  Thu, 21 Nov 2019 13:58:31 +0000

storm (0.19-2) unstable; urgency=low

  * QA upload.

  [ Scott Kitterman ]
  * QA upload to orphan the package, no human maintainers, see #887813
  * Fixed watch file to work with Launchpad again. (Closes: #664743).
  * Change priority to optional
  * Delete DPMT Vcs-* since package is no longer team maintained
  * Add missing build-depends to fix FTBFS (Closes: #887748)
  * Add dh-python to build-depends

  [ Miguel Landaeta ]
  * Bump Standards-Version to 3.9.8. No changes were required.
  * Remove myself from uploaders list. (Closes: #871889)
  * Bump DH compat level to 10.
  * Wrap and sort dependencies lists.
  * Update copyright info.

 -- Scott Kitterman <scott@kitterman.com>  Sat, 20 Jan 2018 01:23:12 -0500

storm (0.19-1) unstable; urgency=low

  * New upstream release.
  * Update copyright dates.

 -- Miguel Landaeta <miguel@miguel.cc>  Thu, 10 Nov 2011 19:03:56 -0430

storm (0.18-1) unstable; urgency=low

  * Initial release in Debian. (Closes: #511253).
  * Bump Standards-Version to 3.9.2. No changes were required.
  * Set Debian Python Modules Team as Maintainer and add myself as Uploader.
  * Add Vcs-* fields.
  * Remove unnecessary ${python:Provides} and pycompat file.
  * Fix Suggests list.
  * Add python-storm-dbg package.
  * Make copyright file DEP-5 compliant.
  * Pass --buildsystem=python_distutils option to debhelper and remove
    unnecessary overrides in rules file.

 -- Miguel Landaeta <miguel@miguel.cc>  Tue, 31 May 2011 19:46:50 -0430

storm (0.18-0ubuntu1) oneiric; urgency=low

  * New upstream release
  * Switch to source format 3.0 (quilt) and .bz2 tarball
  * Switch to dh_python2
    - Drop build-depends on python-central
    - Bump python-all-dev version requirement
    - Update debian/rules
  * Switch from CDBS to DH based debian/rules
    - Drop cdbs build-dep
    - Update debian/rules
    - Bump debian/compat to 7
  * Drop obsolete XB-Python-Version
  * Change XS-Python-Version to X-Python-Version

 -- Scott Kitterman <scott@kitterman.com>  Wed, 04 May 2011 17:32:20 -0400

storm (0.15-0ubuntu2) natty; urgency=low

  * Rebuild to add support for python 2.7.

 -- Matthias Klose <doko@ubuntu.com>  Fri, 03 Dec 2010 00:15:02 +0000

storm (0.15-0ubuntu1) karmic; urgency=low

  * New upstream release.
  * debian/control:
    - Make python-storm architecture-dependent, as Storm builds extensions
      now. (LP: #338420)
    - Add ${shlibs:Depends} to python-storm's Depends field.
  * debian/rules:
    - Update version number.
    - Remove no longer useful DH_ALWAYS_EXCLUDE -- we need /usr/lib now.
  * debian/copyright:
    - Update upstream copyright dates.
    - Replace '(C)' in Debian packaging copyright notice with 'copyright',
      which is actually legally meaningful.

 -- William Grant <wgrant@ubuntu.com>  Tue, 18 Aug 2009 10:40:00 +1000

storm (0.14-0ubuntu2) jaunty; urgency=low

  * Really update to the new upstream version. The previous upload had a
    reversion to 0.12 in the diff.gz. (LP: #364405)

 -- William Grant <wgrant@ubuntu.com>  Tue, 21 Apr 2009 12:08:58 +1000

storm (0.14-0ubuntu1) jaunty; urgency=low

  * New upstream version.
  * Watch file added

 -- Morten Kjeldgaard <mok0@ubuntu.com>  Fri, 16 Jan 2009 10:43:56 +0100

storm (0.12-0ubuntu2) intrepid; urgency=low

  * Added python-psycopg2, python-mysqldb and python-pysqlite2 to Suggests of
    python-storm (LP: #259883).

 -- Alessio Treglia <quadrispro@ubuntu.com>  Thu, 21 Aug 2008 19:19:29 +0200

storm (0.12-0ubuntu1) hardy; urgency=low

  [ Morten Kjeldgaard ]
  * New upstream release, fixes several bugs (LP: #156100)
    - debian/rules: get-orig-source target added. Avoid packing
      usr/lib
    - debian/control: move to section python, move http stanza
      to Homepage field.

  [ Scott Kitterman ]
  * Change get-orig-source to use production launchpad URL (remove edge)

 -- Morten Kjeldgaard <mok@bioxray.au.dk>  Mon, 25 Feb 2008 15:35:09 +0100

storm (0.11-0ubuntu2) hardy; urgency=low

  * Fix subst variable in debian/control to make python-storm install properly
    again.
  * Don't run prerm if upgrading from 0.11-0ubuntu1.

 -- Soren Hansen <soren@ubuntu.com>  Fri, 11 Jan 2008 17:39:39 +0100

storm (0.11-0ubuntu1) hardy; urgency=low

  * New upstream release
  * Switch to python-central.

 -- Soren Hansen <soren@ubuntu.com>  Thu, 10 Jan 2008 17:23:53 +0100

storm (0.9-0ubuntu1) gutsy; urgency=low

  * Initial release.

 -- Soren Hansen <soren@ubuntu.com>  Fri, 13 Jul 2007 09:38:21 +0100
